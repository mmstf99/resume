// Scroll Animation
const observer = new IntersectionObserver((entries) => {
    entries.forEach(entry => {
        if (entry.isIntersecting) {
            entry.target.style.opacity = 1;
            entry.target.style.transform = 'translateY(0)';
        }
    });
});

document.querySelectorAll('.timeline-item, .card').forEach(el => {
    el.style.opacity = 0;
    el.style.transform = 'translateY(30px)';
    el.style.transition = 'all 0.5s ease-out';
    observer.observe(el);
});

// Smooth scroll for navbar links
document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();
        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});

// document.querySelector('#website').addEventListener('mouseover', (e) => {
//     e.preventDefault();
//
//     document.querySelector('#website').innerHTML = "<i class=\"fa-solid fa-globe\"></i> profile.mstf.uz"
// })
//
// document.querySelector('#website').addEventListener('mouseleave', (e) => {
//     e.preventDefault();
//
//     document.querySelector('#website').innerHTML = "<i class=\"fa-solid fa-globe\"></i> Website"
// })
//
// document.querySelector('#phone').addEventListener('mouseover', (e) => {
//     e.preventDefault();
//
//     document.querySelector('#phone').innerHTML = "<i class=\"fa-solid fa-phone\"></i> +998-91-341-85-52"
// })
//
// document.querySelector('#phone').addEventListener('mouseleave', (e) => {
//     e.preventDefault();
//
//     document.querySelector('#phone').innerHTML = "<i class=\"fa-solid fa-phone\"></i> Phone"
// })
//
// document.querySelector('#telegram').addEventListener('mouseover', (e) => {
//     e.preventDefault();
//
//     document.querySelector('#telegram').innerHTML = "<i class=\"fa-brands fa-telegram\"></i> mmstf99"
// })
//
// document.querySelector('#telegram').addEventListener('mouseleave', (e) => {
//     e.preventDefault();
//
//     document.querySelector('#telegram').innerHTML = "<i class=\"fa-brands fa-telegram\"></i> Telegram"
// })

const contacts = [
    {
        type: "website",
        iconClass: "fa-solid fa-globe",
        originalText: "Website",
        activatedText: "profile.mstf.uz"
    },
    {
        type: "phone",
        iconClass: "fa-solid fa-phone",
        originalText: "Phone",
        activatedText: "+998-91-341-85-52"
    },
    {
        type: "telegram",
        iconClass: "fa-brands fa-telegram",
        originalText: "Telegram",
        activatedText: "@mmstf99"
    },
]

for (const contact of contacts) {
    document.getElementById(`${contact.type}`).addEventListener('mouseover', (e) => {
        e.preventDefault();

        document.getElementById(`${contact.type}`).innerHTML = `<i class="${contact.iconClass}"></i> ${contact.activatedText}`
    })

    document.getElementById(`${contact.type}`).addEventListener('mouseleave', (e) => {
        e.preventDefault();

        document.getElementById(`${contact.type}`).innerHTML = `<i class="${contact.iconClass}"></i> ${contact.originalText}`
    })
}